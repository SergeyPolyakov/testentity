﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Contract;
using Domain;
using Database;
using Repository;

namespace UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DefaultContext _context = new DefaultContext();
        IRepository<Employee> _employeeRepository;
        IRepository<Departament> _departamentRepository;
        IRepository<Organization> _organizationRepository;
        IRepository<Picture> _pictureRepository;
        IRepository<User> _userRepository;
        IRepository<Domain.Roles> _roleRepository;

        //public UnitOfWork(DefaultContext context)
        //{
        //    _context = context;
        //}
        public IRepository<Employee> EmployeeRepository
        {
            get
            {
                if (_employeeRepository == null)
                {
                    _employeeRepository = new Repository<Employee>(_context);
                }
                return _employeeRepository;
            }
        }

        public IRepository<Departament> DepartamentRepository
        {
            get
            {
                if (_departamentRepository == null)
                {
                    _departamentRepository = new Repository<Departament>(_context);
                }
                return _departamentRepository;
            }
        }

        public IRepository<Organization> OrganizationRepository
        {
            get
            {
                if (_organizationRepository == null)
                {
                    _organizationRepository = new Repository<Organization>(_context);
                }
                return _organizationRepository;
            }
        }

        public IRepository<Picture> PictureRepository
        {
            get
            {
                if (_pictureRepository == null)
                {
                    _pictureRepository = new Repository<Picture>(_context);
                }
                return _pictureRepository;
            }
        }

        public IRepository<Domain.Roles> RoleRepository
        {
            get
            {
                if (_roleRepository == null)
                {
                    _roleRepository = new Repository<Roles>(_context);
                }

                return _roleRepository;
            }
        }

        public IRepository<User> UserReposityory
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new Repository<User>(_context);
                }
                return _userRepository;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
