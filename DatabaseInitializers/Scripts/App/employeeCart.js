﻿$(function () {
    $('#BirthDate').datepicker();

    $('#OrganizationId').change(function () {
        // получаем выбранный id
        var id = $(this).val();

        $.ajax({
            type: 'POST',
            url: '/Employees/DepartamentsList',
            data: { 'orgId': id },
            success: function (data) {

                // заменяем содержимое присланным частичным представлением
                $('#DepartamentId').replaceWith(data);
            }
        });
    });
})