$(document).ready(function () {
    $('.birthdate').datepicker();

    $('[data-autocomplete-source]').each(function () {
        var target = $(this);
        target.autocomplete({ source: target.attr('data-autocomplete-source')});
    });

    $('#submit').click(function (e) {
        e.preventDefault();

        var fullName = $('#search').val();
        var regexp = '/[�-ߨ�-��]+\s+[�-ߨ�-��]+|[�-ߨ�-��]+/u';
        var result = fullName.match(regexp);
        var space = ' ';
        var spaceExist = result[0].indexOf(space);
        var arr = result[0].split(' ');
        var name = '';
        var surname = '';

        if (spaceExist !== -1) {
            for (var i = 0; i < arr.length; i++) {

                if (arr[i].length > 1) {

                    if (name.length > 0) {
                        surname = arr[i];
                        break;
                    }

                    if (name.length === 0) {
                        name = arr[i];
                    }
                }
            }

            name = encodeURIComponent(name);
            surname = encodeURIComponent(surname);

        } else {
            name = result[0];
            surname = result[0];
            name = encodeURIComponent(name);
            surname = encodeURIComponent(surname);
        }

        $('#reset').css('visibility', 'visible');

        $('.data').load('/Employees/EmployeeSearch?name=' + name + '&surname=' + surname);
    })
});