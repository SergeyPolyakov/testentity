﻿using System.Collections.Generic;
using EmployeeDeskBook.Models;
using Domain;

namespace EmployeeDeskBook.App_Start
{
    public static class MappingConfig
    {
        public static void RegisterMaps()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<Pager, EmployeeFilteringViewModel>()
                .ForMember(dest => dest.Pager, m => m.MapFrom(src => src))
                .ForMember(dest => dest.Employees, m => m.Ignore())
                .ForMember(dest => dest.FromSortDate, m => m.Ignore())
                .ForMember(dest => dest.ToSortDate, m => m.Ignore())
                .ForMember(dest => dest.DepartamentId, m => m.Ignore());

                config.CreateMap<IEnumerable<Employee>, EmployeeFilteringViewModel>()
                .ForMember(dest => dest.Employees, m => m.MapFrom(src => src))
                .ForMember(dest => dest.Pager, m => m.Ignore())
                .ForMember(dest => dest.FromSortDate, m => m.Ignore())
                .ForMember(dest => dest.ToSortDate, m => m.Ignore())
                .ForMember(dest => dest.DepartamentId, m => m.Ignore());


                config.CreateMap<Pager, DepartamentIndexViewModel>()
                .ForMember(dest => dest.Pager, m => m.MapFrom(src => src))
                .ForMember(dest => dest.Departaments, m => m.Ignore());

                config.CreateMap<IEnumerable<Departament>, DepartamentIndexViewModel>()
                .ForMember(dest => dest.Departaments, m => m.MapFrom(src => src))
                .ForMember(dest => dest.Pager, m => m.Ignore());


                config.CreateMap<Pager, OrganizationIndexViewModel>()
                .ForMember(dest => dest.Pager, m => m.MapFrom(src => src))
                .ForMember(dest => dest.Organizations, m => m.Ignore());

                config.CreateMap<IEnumerable<Organization>, OrganizationIndexViewModel>()
                .ForMember(dest => dest.Organizations, m => m.MapFrom(src => src))
                .ForMember(dest => dest.Pager, m => m.Ignore());
            });
        }
    }
}