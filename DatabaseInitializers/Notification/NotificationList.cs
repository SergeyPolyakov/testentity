﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace EmployeeDeskBook.Notification
{
    public static class NotificationList    
    {
        private static List<INotification> _notifications;

        static NotificationList()
        {
            _notifications = new List<INotification>();
            Count = 0;
        }

        private static int _count;
        public static int Count
        {
            get
            {
                return _count;
            }
            private set
            {
                _count = value;
            }
        }
        public static void Add(INotification notifications)
        {
            _notifications.Add(notifications);
            NotificationList.Count = _notifications.Count;
        }

        public static List<INotification> GetAll()
        {
            List<INotification> notifiyes = new List<INotification>();
            foreach (var notifi in _notifications)
            {
                notifiyes.Add(notifi);
            }
            _notifications.Clear();
            return notifiyes;
        }
    }
}