﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeDeskBook.Notification
{
    public enum NotificationType
    {
        Notice,
        Error,
        Warning
    }

    public class Notification : INotification
    {
        string _message;

        public string Message
        {
            get
            {
                return _message;
            }

        }

        NotificationType _type;

        public NotificationType Type
        {
            get
            {
                return _type;
            }
        }

        public Notification(NotificationType type, string message)
        {
            _type = type;
            _message = message;
        }
    }
}