﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeDeskBook.Notification
{
    public interface INotification
    {
        string Message { get; }
        NotificationType Type { get; }
    }
}