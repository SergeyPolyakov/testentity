﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using EmployeeDeskBook.Models;
using System.IO;
using System.Text.RegularExpressions;
using EmployeeDeskBook.Notification;
using Domain;
using Contract;
using AutoMapper;

namespace EmployeeDeskBook.Controllers
{
    [Authorize]
    public class EmployeesController : Controller
    {

        IUnitOfWork _uow;
      
        public EmployeesController(IUnitOfWork uow )
        {
            _uow = uow;

        }

        public ActionResult Index(EmployeeFilteringViewModel model, int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);


            IEnumerable<Employee> employees = _uow.EmployeeRepository.GetWithInclude(o => o.Organization,e => e.Departament);
            Notification.Notification notifi = null;

            IList<Departament> departaments = _uow.DepartamentRepository.GetItemsList().ToList();
            departaments.Insert(0, new Departament { Id = 0, Name = "Все" });
            ViewBag.Departaments = new SelectList(departaments, "Id", "Name");

            if (model.DepartamentId != null && model.DepartamentId != 0)
            {
                int depId = Convert.ToInt32(model.DepartamentId);
                employees = _uow.EmployeeRepository.GetWithInclude(e => e.Organization, e => e.Departament);
                employees = employees.Where(e => e.DepartamentId == depId).OrderBy(e => e.Id);
            }

            if (!String.IsNullOrEmpty(model.FromSortDate) && !String.IsNullOrEmpty(model.ToSortDate))
            {
                DateTime fromDate;
                DateTime toDate;
                bool corretDate = true;
                if (DateTime.TryParse(model.FromSortDate, out fromDate) == false)
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка!Введите корректные данные дат для фильтрации");
                    NotificationList.Add(notifi);
                    ModelState.AddModelError("FromSortDate", "");
                    corretDate = false;
                }
                if (DateTime.TryParse(model.ToSortDate, out toDate) == false)
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка!Введите корректные данные дат для фильтрации");
                    NotificationList.Add(notifi);
                    ModelState.AddModelError("ToSortDate", "");
                    corretDate = false;
                }


                if (corretDate)
                {
                    employees = employees.Where(e => e.BirthDate >= fromDate && e.BirthDate <= toDate);
                }
            }
            else if (!String.IsNullOrEmpty(model.FromSortDate))
            {
                DateTime fromDate;
                if (DateTime.TryParse(model.FromSortDate, out fromDate))
                {
                    employees = employees.Where(e => e.BirthDate >= fromDate);
                }
                else
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка!Введите корректные данные дат для фильтрации");
                    NotificationList.Add(notifi);
                    ModelState.AddModelError("FromSortDate", "");
                }
            }
            else if (!String.IsNullOrEmpty(model.ToSortDate))
            {
                DateTime toDate;
                if (DateTime.TryParse(model.ToSortDate, out toDate))
                {
                    employees = employees.Where(e => e.BirthDate <= toDate);
                }
                else
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка!Введите корректные данные дат для фильтрации");
                    NotificationList.Add(notifi);
                    ModelState.AddModelError("ToSortDate", "");
                }
            }


            IEnumerable<Employee> employeesPager = employees.OrderBy(e => e.Id).Skip<Employee>((pageNumber - 1) * pageSize).Take<Employee>(pageSize);
            Pager pager = new Pager { PageNumber = pageNumber, PageSize = pageSize, TotalItems = employees.Count<Employee>() };

            model = Mapper.Map<IEnumerable<Employee>, EmployeeFilteringViewModel>(employeesPager, model);
            model = Mapper.Map<Pager, EmployeeFilteringViewModel>(pager, model);

            return View(model);

        }

        [HttpPost]
        public ActionResult DepartamentsList(int orgId)
        {
            Organization organization = _uow.OrganizationRepository.GetItemsList().FirstOrDefault(e => e.Id == orgId);
            List<Departament> departaments = organization.Departaments.ToList<Departament>();
            return PartialView(departaments);
        }
        public ActionResult AutocompleteSearch(string term)
        {
            string text = null;
            Regex regex = new Regex(@"[А-ЯЁа-яё]+");

            List<string> fullNames = new List<string>();
            List<Employee> employees = null;
            MatchCollection mc = regex.Matches(term);

            if (mc.Count == 1)
            {
                string name = mc[0].Value;
                employees = _uow.EmployeeRepository.GetItemsList().Where(e => e.Name.Contains(name) || e.Surname.Contains(name)).ToList();
            }
            else if (mc.Count == 2)
            {
                string name = mc[0].Value;
                string surname = mc[1].Value;

                employees = _uow.EmployeeRepository.GetItemsList().Where(e => e.Name.Contains(name) || e.Surname.Contains(surname)).ToList();

            }

            if (mc.Count != 0 && mc.Count <= 2)
            {
                foreach (Employee employee in employees)
                {
                    text += employee.Name;
                    text += " " + employee.Surname;

                    fullNames.Add(text);
                    text = null;
                }

            }

            return Json(fullNames.Distinct<string>(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeeSearch(string name, string surname)
        {
            IEnumerable<Employee> employees = null;
            if (name.Equals(surname))
            {
               
                employees = _uow.EmployeeRepository.GetWithInclude(e => e.Organization.Departaments).Where(c => c.Name.Contains(name) || c.Surname.Contains(surname));
            }
            else
            {
                employees = _uow.EmployeeRepository.GetWithInclude(e => e.Organization.Departaments).Where(a => a.Name.Contains(name) && a.Surname.Contains(surname));
            }
            return PartialView(employees);
        }
        public ActionResult Details(int id)
        {

            Employee employee = _uow.EmployeeRepository.GetWithInclude(e => e.Picture, o => o.Organization, d => d.Departament).FirstOrDefault(e => e.Id == id);

            if (employee == null)
            {
                return HttpNotFound();
            }

            if (employee.Picture != null)
            {
                byte[] image = employee.Picture.Image;
                ViewBag.Image = image;

            }

            return View(employee);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            int selectedIndex = 1;
            List<Organization> dbOrganizations = _uow.OrganizationRepository.GetItemsList().ToList<Organization>();
            SelectList organizations = new SelectList(dbOrganizations, "Id", "Name", selectedIndex);
            Organization organization = dbOrganizations.First();

            ViewBag.Organizations = organizations;
            SelectList departaments = new SelectList(organization.Departaments, "Id", "Name");
            ViewBag.Departaments = departaments;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Create(Employee employee, HttpPostedFileBase uploadImage)
        {
            int selectedIndex = 1;
            Organization organization = _uow.OrganizationRepository.GetItemsList().FirstOrDefault(o => o.Id == selectedIndex);
            SelectList organizations = new SelectList(_uow.OrganizationRepository.GetItemsList(), "Id", "Name", selectedIndex);
         
            ViewBag.Organizations = organizations;
            SelectList departaments = new SelectList(organization.Departaments, "Id", "Name");
            ViewBag.Departaments = departaments;

            Picture picture = _uow.PictureRepository.GetItemsList().FirstOrDefault(e => e.Id == employee.Id);
            if (picture != null)
            {
                byte[] image = picture.Image;
                ViewBag.Image = image;

            }

            Regex regex = new Regex(@".jpg|.JPEG|.jpeg|.png|.gif");
            Notification.Notification notifi = null;
            bool validImage = true;

            if (uploadImage != null)
            {
                string extension = Path.GetExtension(uploadImage.FileName);
                validImage = regex.IsMatch(extension);
            }

            if (ModelState.IsValid && validImage)
            {
                try
                {
                    _uow.EmployeeRepository.Create(employee);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о сотруднике");
                    NotificationList.Add(notifi);
                    return View(employee);
                }

                if (uploadImage != null)
                {
                    byte[] image = null;
                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        image = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }
                    Picture pic = new Picture();
                    pic.Image = image;
                    pic.Id = employee.Id;
                    try
                    {
                       _uow.PictureRepository.Create(pic);
                       _uow.Save();
                    }
                    catch
                    {
                        notifi = new Notification.Notification(NotificationType.Error, "Ошибка при сохранении фотографии сотрудника");
                        NotificationList.Add(notifi);
                        return View(employee);
                    }
                }

                notifi = new Notification.Notification(NotificationType.Notice, "Сотрудник успешно создан");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            if (validImage == false)
            {
                notifi = new Notification.Notification(NotificationType.Error, "Неверный формат изображения, проверьте загружаемое изображеие");
                NotificationList.Add(notifi);
            }
            else
            {
                notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
                NotificationList.Add(notifi);

            }

            return View(employee);
        }

        [Authorize(Roles = "admin, moderator")]
        public ActionResult Edit(int id)
        {
            int selectedIndex = 1;
            List<Organization> dbOrganizations = _uow.OrganizationRepository.GetItemsList().ToList<Organization>();
            SelectList organizations = new SelectList(dbOrganizations, "Id", "Name", selectedIndex);
            Organization organization = dbOrganizations.First();
            ViewBag.Organizations = organizations;
            SelectList departaments = new SelectList(organization.Departaments, "Id", "Name");
            ViewBag.Departaments = departaments;

            Employee employee = _uow.EmployeeRepository.GetWithInclude(e => e.Picture).First(e => e.Id == id);


            if (employee == null)
            {
                return HttpNotFound();
            }

            if (employee.Picture != null)
            {
                byte[] image = employee.Picture.Image;
                ViewBag.Image = image;

            }
            return View(employee);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin, moderator")]
        public ActionResult Edit(Employee employee, HttpPostedFileBase uploadImage, int? pictureId)
        {
            int selectedIndex = 1;
            List<Organization> dbOrganizations = _uow.OrganizationRepository.GetItemsList().ToList<Organization>();
            SelectList organizations = new SelectList(dbOrganizations, "Id", "Name", selectedIndex);
            Organization organization = dbOrganizations.First();
            ViewBag.Organizations = organizations;
            SelectList departaments = new SelectList(organization.Departaments, "Id", "Name");
            ViewBag.Departaments = departaments;

            Notification.Notification notifi = null;

            if (pictureId != null && employee == null)
            {
                Picture photo = _uow.PictureRepository.GetItemsList().FirstOrDefault(e => e.Id == pictureId);
                try
                {
                    _uow.PictureRepository.Delete(photo.Id);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка удаления фотографии");
                    NotificationList.Add(notifi);
                }

                notifi = new Notification.Notification(NotificationType.Notice, "Фотография успешно удалена");
                NotificationList.Add(notifi);
                return View(employee);


            }

            Picture picture = _uow.PictureRepository.GetItem(employee.Id);
            if (picture != null)
            {
                byte[] image = picture.Image;
                ViewBag.Image = image;

            }

            Regex regex = new Regex(@".jpg|.JPEG|.jpeg|.png|.gif");

            bool validImage = true;

            if (uploadImage != null)
            {
                string extension = Path.GetExtension(uploadImage.FileName);
                validImage = regex.IsMatch(extension);
            }

            if (ModelState.IsValid && validImage)
            {
                if (uploadImage != null)
                {
                
                    byte[] image = null;

                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        image = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }

                    Picture employeePicture = _uow.PictureRepository.GetItem(employee.Id);

                    try
                    {
                        if (employeePicture == null)
                        {

                            Picture pic = new Picture();
                            pic.Id = employee.Id;
                            pic.Image = image;
                            _uow.PictureRepository.Create(pic);
                            _uow.Save();

                        }
                        else
                        {
                            picture.Id = employee.Id;
                            picture.Image = image;
                            _uow.PictureRepository.Update(picture);
                            _uow.Save();
                        }
                    }
                    catch
                    {
                        notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения фотографии сотрудника");
                        NotificationList.Add(notifi);
                        return View(employee);
                    }

              
                }


                try
                {

                    _uow.EmployeeRepository.Update(employee);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения информации о сотруднике");
                    NotificationList.Add(notifi);
                    return View(employee);
                }
                notifi = new Notification.Notification(NotificationType.Notice, "Информация о сотруднике успешно отредактирована");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            if (validImage == false)
            {
                notifi = new Notification.Notification(NotificationType.Error, "Неверный формат изображения, проверьте загружаемое изображеие");
                NotificationList.Add(notifi);
            }
            else
            {
                notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
                NotificationList.Add(notifi);

            }

            return View(employee);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            Employee employee = _uow.EmployeeRepository.GetWithInclude(e => e.Picture, o => o.Organization, d => d.Departament).FirstOrDefault(e => e.Id == id);

            if (employee == null)
            {
                return HttpNotFound();
            }

            if (employee.Picture != null)
            {
                byte[] image = employee.Picture.Image;
                ViewBag.Image = image;

            }
            return View(employee);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Notification.Notification notifi = null;

            Employee employee = _uow.EmployeeRepository.GetWithInclude(e => e.Picture, o => o.Organization, d => d.Departament).FirstOrDefault(e => e.Id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }

            try
            {
                _uow.EmployeeRepository.Delete(employee.Id);
                _uow.Save();
            }
            catch(Exception e)
            {
                notifi = new Notification.Notification(NotificationType.Error, "Ошибка удаления сотрудника");
                NotificationList.Add(notifi);
                return View(employee);
            }

            notifi = new Notification.Notification(NotificationType.Notice, "Сотрудник успешно удален");
            NotificationList.Add(notifi);
            return RedirectToAction("Index");
        }




    }
}