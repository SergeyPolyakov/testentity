﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EmployeeDeskBook.Models;
using EmployeeDeskBook.Notification;
using Domain;
using Contract;
using AutoMapper;

namespace EmployeeDeskBook.Controllers
{
    [Authorize(Roles = "admin")]
    public class DepartamentController : Controller
    {

        IUnitOfWork _uow;
        public DepartamentController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public ActionResult Index(int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            IEnumerable<Departament> departaments = _uow.DepartamentRepository.GetItemsList();
            IEnumerable<Departament> departamentsOnPage = departaments.Skip<Departament>((pageNumber - 1) * pageSize).Take<Departament>(pageSize);

            Pager pager = new Pager { PageNumber = pageNumber, PageSize = pageSize, TotalItems = departaments.Count<Departament>() };
            DepartamentIndexViewModel divm = Mapper.Map<IEnumerable<Departament>, DepartamentIndexViewModel>(departamentsOnPage);
            divm = Mapper.Map<Pager, DepartamentIndexViewModel>(pager, divm);
            return View(divm);
        }

        public ActionResult Details(int id)
        {
            Departament departament = _uow.DepartamentRepository.GetItemsList().FirstOrDefault(o => o.Id == id);
            if (departament == null)
            {
                return HttpNotFound();
            }
            return View(departament);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Departament departament)
        {
            Notification.Notification notifi = null;
         
            if (ModelState.IsValid)
            {
                try
                {
                    _uow.DepartamentRepository.Create(departament);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о организации");
                    NotificationList.Add(notifi);
                    return View(departament);
                }
                notifi = new Notification.Notification(NotificationType.Notice, "Организация создана успешно");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
            NotificationList.Add(notifi);
            return View(departament);
        }

        public ActionResult Edit(int id)
        {
            Departament departament = _uow.DepartamentRepository.GetItemsList().FirstOrDefault(e => e.Id == id);
            return View(departament);
        }

        [HttpPost]
        public ActionResult Edit(Departament departament)
        {
            
            Notification.Notification notifi = null;

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.DepartamentRepository.Update(departament);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о департаменте");
                    NotificationList.Add(notifi);
                    return View(departament);
                }

                notifi = new Notification.Notification(NotificationType.Notice, "Иформация о департаменте успешно сохранена");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
            NotificationList.Add(notifi);
            return View(departament);
        }

        public ActionResult Delete(int id)
        {
            Departament departament = _uow.DepartamentRepository.GetItemsList().FirstOrDefault(e => e.Id == id);
            if (departament == null)
            {
                return HttpNotFound();
            }
            return View(departament);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult ConfirmDelete(int id)
        {
            Notification.Notification notifi = null;
            Departament departament = _uow.DepartamentRepository.GetItemsList().FirstOrDefault(e => e.Id == id);
            try
            {
                _uow.DepartamentRepository.Delete(departament.Id);
                _uow.Save();
            }
            catch(Exception e)
            {
                notifi = new Notification.Notification(NotificationType.Error, "Ошибка удаления департамента");
                NotificationList.Add(notifi);
                return View(departament);
            }
            notifi = new Notification.Notification(NotificationType.Notice, "Департамент успешно удален");
            NotificationList.Add(notifi);
            return RedirectToAction("Index");
        }

    }
}
