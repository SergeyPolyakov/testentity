﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EmployeeDeskBook.Models;
using EmployeeDeskBook.Notification;
using Domain;
using Contract;
using AutoMapper;

namespace EmployeeDeskBook.Controllers
{
    [Authorize(Roles = "admin")]
    public class OrganizationController : Controller
    {

        IUnitOfWork _uow;
        public OrganizationController(IUnitOfWork uow)
        {
            _uow = uow;
        }
  
        public ActionResult Index(int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            IEnumerable<Organization> organizations = _uow.OrganizationRepository.GetItemsList();
            IEnumerable<Organization> organizationsOnPage = organizations.Skip<Organization>((pageNumber - 1) * pageSize).Take<Organization>(pageSize);

            Pager pager = new Pager { PageNumber = pageNumber, PageSize = pageSize, TotalItems = organizations.Count<Organization>() };
            OrganizationIndexViewModel oivm = Mapper.Map<IEnumerable<Organization>, OrganizationIndexViewModel>(organizationsOnPage);
            oivm = Mapper.Map<Pager, OrganizationIndexViewModel>(pager, oivm);
            return View(oivm);
        }


        public ActionResult Details(int id)
        {
            Organization organization = _uow.OrganizationRepository.GetItemsList().FirstOrDefault(o => o.Id == id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

    

        public ActionResult Create()
        {
            MultiSelectList departaments = new MultiSelectList(_uow.DepartamentRepository.GetItemsList(), "Id", "Name");
            ViewBag.Departaments = departaments;
            return View();
        }

   

        [HttpPost]
        public ActionResult Create(Organization organization, List<int> deps)
        {
            MultiSelectList depList = new MultiSelectList(_uow.DepartamentRepository.GetItemsList(), "Id", "Name");
            ViewBag.Departaments = depList;
            Notification.Notification notifi = null;
            if (deps != null)
            {
                foreach (int departament in deps)
                {
                    Departament dep = _uow.DepartamentRepository.GetItemsList().FirstOrDefault(e => e.Id == departament);
                    if (dep != null)
                    {
                        organization.Departaments.Add(dep);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                try
                { 
                   _uow.OrganizationRepository.Create(organization);
                   _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о организации");
                    NotificationList.Add(notifi);
                    return View(organization);
                }
                notifi = new Notification.Notification(NotificationType.Notice, "Организация создана успешно");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
            NotificationList.Add(notifi);
            return View(organization);
        
    }

   

        public ActionResult Edit(int id)
        {
            MultiSelectList departaments = new MultiSelectList(_uow.DepartamentRepository.GetItemsList(), "Id", "Name");
            ViewBag.Departaments = departaments;
            Organization organization = _uow.OrganizationRepository.GetItemsList().FirstOrDefault(e => e.Id == id);
            return View(organization);
        }


        [HttpPost]
        public ActionResult Edit(Organization organization, List<int> deps)
        {          
            MultiSelectList depList = new MultiSelectList(_uow.DepartamentRepository.GetItemsList(), "Id", "Name");
            ViewBag.Departaments = depList;
            Notification.Notification notifi = null;

            Organization changedOrg = _uow.OrganizationRepository.GetItem(organization.Id);
            changedOrg.Name = organization.Name;
            changedOrg.Departaments.Clear();

            if (deps != null)
            {
                foreach (int departament in deps)
                {
                    Departament dep = _uow.DepartamentRepository.GetItem(departament);
                    if (dep != null)
                    {
                        changedOrg.Departaments.Add(dep);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.OrganizationRepository.Update(changedOrg);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о организации");
                    NotificationList.Add(notifi);
                    return View(organization);
                }
                notifi = new Notification.Notification(NotificationType.Notice, "Организация отредактирована успешно");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
            NotificationList.Add(notifi);
            return View(organization);
        }

  

        public ActionResult Delete(int id)
        {
            Organization organization = _uow.OrganizationRepository.GetItemsList().FirstOrDefault(o => o.Id == id);
            if (organization == null)
            {
                return HttpNotFound();
            }

            return View(organization);
        }

       

        [HttpPost, ActionName("Delete")]
        public ActionResult ConfirmDelete(int id)
        {
            Notification.Notification notifi = null;
            Organization organization = _uow.OrganizationRepository.GetItemsList().FirstOrDefault(o => o.Id == id);
            try
            {
                _uow.OrganizationRepository.Delete(organization.Id);
                _uow.Save();
            }
            catch(Exception e)
            {
                notifi = new Notification.Notification(NotificationType.Error, e.Message);
                NotificationList.Add(notifi);
                return View(organization);
            }
            notifi = new Notification.Notification(NotificationType.Notice, "Организация успешно удалена");
            NotificationList.Add(notifi);
            return RedirectToAction("Index");
        }

    }
}
