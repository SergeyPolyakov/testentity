﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Security;
using EmployeeDeskBook.Providers;
using EmployeeDeskBook.Models;
using Domain;
using Contract;
namespace EmployeeDeskBook.Controllers
{
    public class AccountController : Controller
    {
        IUnitOfWork _uow;

        public AccountController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = null;

                user = _uow.UserReposityory.GetItemsList().FirstOrDefault(u => u.Name == model.Name && u.Password == model.Password);

                if(user != null)
                {
                    FormsAuthentication.SetAuthCookie(model.Name, true);
                    return RedirectToAction("Index", "Employees");
                }
                else
                {
                    ModelState.AddModelError("", "Пользователя с таким логином и паролем нет");
                }
            }

            return View(model);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login");
        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _uow.UserReposityory.GetItemsList().FirstOrDefault(u => u.Name == model.Name);

                if (user == null)
                {
                    _uow.UserReposityory.Create(new User { Name = model.Name, Password = model.Password, RoleId = 3 });
                    _uow.Save();

                    user = _uow.UserReposityory.GetItemsList().Where(u => u.Name == model.Name && u.Password == model.Password).FirstOrDefault();

                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(model.Name, true);
                        return RedirectToAction("Index", "Employees");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Пользователья с таким логином существует");
                }
            }

            return View(model);
        }

       

    }
}
