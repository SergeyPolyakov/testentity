﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EmployeeDeskBook.Models;
using EmployeeDeskBook.Notification;
using Contract;
using Domain;


namespace EmployeeDeskBook.Controllers
{
    [Authorize(Roles ="admin")]
    public class AdminController : Controller
    {
        IUnitOfWork _uow;

        public AdminController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public ActionResult Index()
        {
            var users = _uow.UserReposityory.GetWithInclude(u => u.Role).ToList();
            return View(users);
        }

        public ActionResult Create()
        {
            int adminId = 1;

            List<Roles> otherRoles = _uow.RoleRepository.GetItemsList().Where(e => e.Id != adminId).ToList();
            SelectList roles = new SelectList(otherRoles, "Id", "Name");
            ViewBag.Roles = roles;
            return View();
        }


        [HttpPost]
        public ActionResult Create(User user)
        {
            int adminId = 1;

            List<Roles> otherRoles = _uow.RoleRepository.GetItemsList().Where(e => e.Id != adminId).ToList();
            SelectList roles = new SelectList(otherRoles, "Id", "Name");
            ViewBag.Roles = roles;
            Notification.Notification notifi = null;

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.UserReposityory.Create(user);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о пользователе");
                    NotificationList.Add(notifi);
                    return View(user);
                }
                notifi = new Notification.Notification(NotificationType.Notice, "Пользователь создан успешно");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }
            
            notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
            NotificationList.Add(notifi);
            return View(user);
        }


        public ActionResult Edit(int id)
        {
            int adminId = 1;

            User user = _uow.UserReposityory.GetWithInclude(e => e.Role).FirstOrDefault(e => e.Id == id);
            if (user.RoleId == 1)
            {
                List<Roles> role = new List<Roles>();
                role.Add(user.Role);
                SelectList roles = new SelectList(role, "Id", "Name");
                ViewBag.Roles = roles;
            }
            else
            {
                List<Roles> otherRoles = _uow.RoleRepository.GetItemsList().Where(e => e.Id != adminId).ToList();
                SelectList roles = new SelectList(otherRoles, "Id", "Name");
                ViewBag.Roles = roles;
            }
            return View(user);
        }


        [HttpPost]
        public ActionResult Edit(User user)
        {
            int adminId = 1;

            if (user.RoleId == adminId)
            {
                List<Roles> role = new List<Roles>();
                role.Add(user.Role);
                SelectList roles = new SelectList(role, "Id", "Name");
                ViewBag.Roles = roles;
            }
            else
            {
                List<Roles> otherRoles = _uow.RoleRepository.GetItemsList().Where(e => e.Id != adminId).ToList();
                SelectList roles = new SelectList(otherRoles, "Id", "Name");
                ViewBag.Roles = roles;
            }

            Notification.Notification notifi = null;
            
            if (ModelState.IsValid)
            {
                try
                {
                    _uow.UserReposityory.Update(user);
                    _uow.Save();
                }
                catch
                {
                    notifi = new Notification.Notification(NotificationType.Error, "Ошибка сохранения данных о пользователе");
                    NotificationList.Add(notifi);
                    return View(user);
                }

                notifi = new Notification.Notification(NotificationType.Notice, "Иформация о пользователе успешно сохранена");
                NotificationList.Add(notifi);
                return RedirectToAction("Index");
            }

            notifi = new Notification.Notification(NotificationType.Error, "Ошибка, заполните все поля правильно");
            NotificationList.Add(notifi);
            return View(user);
        }


        public ActionResult Delete(int id = 0)
        {
            User user = _uow.UserReposityory.GetWithInclude(e => e.Role).FirstOrDefault(e => e.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Notification.Notification notifi = null;
            User user =_uow.UserReposityory.GetWithInclude(e => e.Role).FirstOrDefault(e => e.Id == id);
            try
            {
                _uow.UserReposityory.Delete(user.Id);
                _uow.Save();
            }
            catch
            {
                notifi = new Notification.Notification(NotificationType.Error, "Ошибка удаления пользователя");
                NotificationList.Add(notifi);
                return View(user);
            }
            notifi = new Notification.Notification(NotificationType.Notice, "Пользователь успешно удален");
            NotificationList.Add(notifi);
            return RedirectToAction("Index");
        }

    }
}
