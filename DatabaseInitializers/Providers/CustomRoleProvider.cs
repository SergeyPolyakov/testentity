﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using EmployeeDeskBook.Models;
using Database;
using Domain;
using Ninject;
using UnitOfWork;
using EmployeeDeskBook.App_Start;

namespace EmployeeDeskBook.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        public  UnitOfWork.UnitOfWork UoW
        {
            get { return NinjectWebCommon.Kernel.Get<UnitOfWork.UnitOfWork>(); }
        }
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] roles = new string[] { };

          
                User user = UoW.UserReposityory.GetItemsList().SingleOrDefault(u => u.Name == username);
                if (user != null)
                {
                    Domain.Roles userRole = UoW.RoleRepository.GetItem(user.RoleId);
                    if (user != null)
                    {
                        roles = new string[] { userRole.Name };
                    }
                }
            

            return roles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            bool inRole = false;

            
                User user = UoW.UserReposityory.GetItemsList().SingleOrDefault(u => u.Name == username);
                if (user != null)
                {
                    Domain.Roles userRole = UoW.RoleRepository.GetItem(user.RoleId);
                    if (userRole != null && userRole.Name == roleName)
                    {
                        inRole = true;
                    }
                }
            

            return inRole;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}