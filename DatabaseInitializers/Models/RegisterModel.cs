﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EmployeeDeskBook.Models
{
    public class RegisterModel
    {
        [DisplayName("Логин")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышена максимальная длина строки")]
        public string Name { get; set; }

        [DisplayName("Пароль")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        public string Password { get; set; }
    }
}