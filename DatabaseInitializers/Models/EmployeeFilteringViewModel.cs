﻿using System.Collections.Generic;
using Domain;
namespace EmployeeDeskBook.Models
{
    public class EmployeeFilteringViewModel
    {
        public IEnumerable<Employee> Employees;
        public Pager Pager;
        public string FromSortDate { get; set; }
        public string ToSortDate { get; set; }
        public int? DepartamentId { get; set; }
    }
}