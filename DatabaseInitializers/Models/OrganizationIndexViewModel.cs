﻿using System.Collections.Generic;
using Domain;
namespace EmployeeDeskBook.Models
{
    public class OrganizationIndexViewModel
    {
        public IEnumerable<Organization> Organizations;
        public Pager Pager;
    }
}