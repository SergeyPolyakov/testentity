﻿using System.Collections.Generic;
using Domain;

namespace EmployeeDeskBook.Models
{
    public class DepartamentIndexViewModel
    {
        public IEnumerable<Departament> Departaments;
        public Pager Pager;
    }
}