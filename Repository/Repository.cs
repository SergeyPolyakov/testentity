﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using Contract;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        DefaultContext _db;
        DbSet<T> _dbSet;
        public Repository(DefaultContext db)
        {

            _db = db;

            _dbSet = _db.Set<T>();
        }

        public void Create(T item)
        {
            _db.Entry(item).State = EntityState.Added;
        }

        public void Delete(int id)
        {
            T item = _dbSet.Find(id);
            if (item != null)
                _dbSet.Remove(item);
        }

        public void Update(T item)
        {
            _dbSet.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
        }

        public T GetItem(int id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<T> GetItemsList()
        {
            return _db.Set<T>().ToList();
        }

        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet.AsNoTracking();
            return includeProperties
            .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }

        public IEnumerable<T> GetWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Where(predicate).ToList();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
