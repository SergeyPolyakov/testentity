﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Contract
{
    public interface IUnitOfWork
    {
        IRepository<Employee> EmployeeRepository { get; }
        IRepository<Departament> DepartamentRepository { get; }
        IRepository<Organization> OrganizationRepository { get; }
        IRepository<Picture> PictureRepository { get; }
        IRepository<User> UserReposityory { get; }
        IRepository<Domain.Roles> RoleRepository { get; }
        void Save();
        void Dispose();
    }
}
