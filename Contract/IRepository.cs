﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    public interface IRepository<T> :IDisposable
    where T : class
    {
        IEnumerable<T> GetItemsList();
        T GetItem(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
        IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
        params Expression<Func<T, object>>[] includeProperties);
        void Save();
    }
}
