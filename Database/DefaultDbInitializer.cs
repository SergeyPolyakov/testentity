﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Domain;

namespace Database
{
    public class DefaultDbInitializer : /*CreateDatabaseIfNotExists*/DropCreateDatabaseAlways<DefaultContext>
    {
        protected override void Seed(DefaultContext context)
            {
            Departament d1 = new Departament() { Id = 1, Name = "Departament1" };
            Departament d2 = new Departament() { Id = 2, Name = "Departament2" };
            Departament d3 = new Departament() { Id = 3, Name = "Departament3" };
            Departament d4= new Departament() { Id = 4, Name = "Departament4" };
            context.Departaments.Add(d1);
            context.Departaments.Add(d2);
            context.Departaments.Add(d3);
            context.Departaments.Add(d4);
            context.SaveChanges();

            context.Organizations.Add(new Organization() { Id = 1, Name = "Газпром", Departaments = new List<Departament>() { d1, d2, d3 } });
            context.SaveChanges();
            context.Organizations.Add(new Organization() { Id = 2, Name = "Госдума", Departaments = new List<Departament>() { d1, d2, d3, d4 } });
            context.SaveChanges();

            Employee e1 = new Employee() { Id = 1, Name = "Станислав", Surname = "Гордеев", BirthDate = DateTime.Parse("01.09.1998"), DepartamentId = 1, OrganizationId = 1};
            Employee e2 = new Employee() { Id = 2, Name = "Дмитрий", Surname = "Овечкин", BirthDate = DateTime.Parse("03.02.1995"), DepartamentId = 2, OrganizationId = 1 };
            Employee e3 = new Employee() { Id = 3, Name = "Сергей", Surname = "Корельский", BirthDate = DateTime.Parse("02.04.1993"), DepartamentId = 3, OrganizationId = 1 };
            Employee e4 = new Employee() { Id = 4, Name = "Владислав", Surname = "Орехов", BirthDate = DateTime.Parse("25.09.1994"), DepartamentId = 3, OrganizationId = 1 };
            context.Employees.Add(e1);
            context.Employees.Add(e2);
            context.Employees.Add(e3);
            context.Employees.Add(e4);
            context.SaveChanges();

           
            context.Roles.Add(new Roles { Id = 1, Name = "admin" });
            context.Roles.Add(new Roles { Id = 2, Name = "moderator" });
            context.Roles.Add(new Roles { Id = 3, Name = "user" });
            context.SaveChanges();

            context.Users.Add(new User { Id = 1, Name = "admin", Password = "admin", RoleId = 1});
            context.Users.Add(new User { Id = 2, Name = "vlad@gmail.com", Password = "vlad123", RoleId = 2 });
            context.Users.Add(new User { Id = 2, Name = "dima@gmail.com", Password = "dima29", RoleId = 3 });
            context.SaveChanges();
        }
    }
}