﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Domain;

namespace Database
{
    public class DefaultContext : DbContext
    {
        public DefaultContext()
            : base("EmployeeConnection")
        {
            System.Data.Entity.Database.SetInitializer(new DefaultDbInitializer());
            this.Database.CommandTimeout = 60;
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Departament> Departaments { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Domain.Roles> Roles { get; set; }
        public DbSet<Picture> Pictures { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Picture>()
                .HasRequired(e => e.Employee)
                .WithOptional(s => s.Picture)
                .WillCascadeOnDelete(true);
        }

    }
}
