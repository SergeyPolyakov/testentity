﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Departament
    {
        public int Id { get; set; }

        [DisplayName("Название департамента")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        public string Name { get; set; }

        public virtual ICollection<Organization> Organizations { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }

        public Departament()
        {
            Organizations = new List<Organization>();
            Employees = new List<Employee>();
        }
    }
}
