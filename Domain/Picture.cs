﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Picture
    { 
        [ForeignKey("Employee")]
        public int Id { get; set; }

        public byte[] Image { get; set; }

        public Employee Employee { get; set; }
    }
}
