﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class User
    {
        public int Id { get; set; }

        [DisplayName("Имя")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышена максимальная длина строки")]
        public string Name { get; set; }

        [DisplayName("Пароль")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышена максимальная длина строки")]
        public string Password { get; set; }

        public int RoleId { get; set; }
        public Roles Role { get; set; }
    }
}
