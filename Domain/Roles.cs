﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Domain
{
    public class Roles
    {
        public int Id { get; set; }

        [DisplayName("Роль")]
        public string Name { get; set; }
    }
}
