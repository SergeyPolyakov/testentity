﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Organization
    {
        public int Id { get; set; }
        [DisplayName("Название организации")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        public string Name { get; set; }
        public virtual ICollection<Departament> Departaments { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }

        public Organization()
        {
            Departaments = new List<Departament>();
            Employees = new List<Employee>();
        }
    }
}
