﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Employee
    {
        public int Id { get; set; }

        [DisplayName("Имя")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышена максимальная длина строки")]
        public string Name { get; set; }

        [DisplayName("Фамилия")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышена максимальная длина строки")]
        public string Surname { get; set; }

        [DisplayName("Дата рождения")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле обязательно для ввода")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        public int? OrganizationId { get; set; }
        public Organization Organization { get; set; }
        public int? DepartamentId { get; set; }
        public Departament Departament { get; set; }

        public Picture Picture { get; set; }
    }
}
